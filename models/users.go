package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Represents a user, we uses bson keyword to tell the mgo driver how to name
// the properties in mongodb document

type ResponseModel struct {
	Message string `bson:"message" json:"message"`
	Status  int    `bson:"status" json:"status"`
}

type DeviceModel struct {
	ID         bson.ObjectId `bson:"_id" json:"id"`
	FacebookID string        `bson:"facebookID" json:"facebookID"`
	DeviceID   string        `bson:"deviceID" json:"deviceID"`
	DeviceType string        `bson:"deviceType" json:"deviceType"`
	OverTaken  int           `bson:"overTaken" json:"overTaken"`
	UpdateAt   time.Time     `bson:"updateAt" json:"updateAt"`
	CreateAt   time.Time     `bson:"createAt" json:"createAt"`
}

type User struct {
	ID    bson.ObjectId `bson:"_id" json:"id"`
	Name  *string       `bson:"name" json:"name"`
	Age   *int          `bson:"age" json:"age"`
	Email *string       `bson:"email" json:"email"`
}

type UserModel struct {
	ID              bson.ObjectId `bson:"_id" json:"id"`
	DeviceType      string        `bson:"deviceType" json:"deviceType"`
	DeviceID        string        `bson:"deviceID" json:"deviceID"`
	Name            string        `bson:"name" json:"name"`
	Gender          string        `bson:"gender" json:"gender"`
	Age             int           `bson:"age" json:"age"`
	Telephone       string        `bson:"telephone" json:"telephone"`
	TelegramID      string        `bson:"telegramID" json:"telegramID"`
	FacebookFdCount int           `bson:"facebookFdCount" json:"facebookFdCount"`
	FacebookID      string        `bson:"facebookID" json:"facebookID"`

	WorkUserModel `bson:"workUserModel" json:"workUserModel"`
	UserRentModel `bson:"userRentModel" json:"userRentModel"`

	Status         int       `bson:"status" json:"status"`
	ShowInRentList int       `bson:"showInRentList" json:"showInRentList"` // 如果唔tick,就唔會比人搵到
	LastOnlineAt   time.Time `bson:"lastOnlineAt" json:"lastOnlineAt"`
	UpdateAt       time.Time `bson:"updateAt" json:"updateAt"`
	CreateAt       time.Time `bson:"createAt" json:"createAt"`
}

type WorkUserModel struct {
	WorkType        string `bson:"workType" json:"workType"`
	WorkSalaryRange int    `bson:"workSalaryRange" json:"workSalaryRange"` // 月入0-5k,5k-10k,10k-15k,
	WorkPlace       string `bson:"workPlace" json:"workPlace"`
	WorkMinTime     string `bson:"workMinTime" json:"workMinTime"`
	WorkMaxTime     string `bson:"workMaxTime" json:"workMaxTime"`
	WorkWeek        string `bson:"workWeek" json:"workWeek"`
}

type UserRentModel struct {
	RentExpectMinCost int    `bson:"rentExpectMinCost" json:"rentExpectMinCost"`
	RentExpectMaxCost int    `bson:"rentExpectMaxCost" json:"rentExpectMaxCost"`
	RentPlace         string `bson:"rentPlace" json:"rentPlace"`
	RentDuration      string `bson:"rentDuration" json:"rentDuration"`
}

type UserDetail struct {
	DeviceModel   `bson:"device" json:"device"`
	UserModel     `bson:"user" json:"user"`
	ResponseModel `bson:"response" json:"response"`
}
