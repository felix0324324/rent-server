package helper

import (
	"fmt"
	"strconv"
	"time"
)

// Get Current Time With +8hrs
func GetCurrentTime(s string) time.Time {
	var currentTime, _ = time.LoadLocation("Asia/Shanghai") //上海
	var nowTime = time.Now().In(currentTime)
	return nowTime
}

// "123" -> 123
// func renewStringOrNull(aString string) string {
// 	var renewString = nil
// 	if len(aString) > 0 {
// 		renewString = aString
// 	}
// 	return renewString
// }

// "123" -> 123
func GetIntFromString(aString string) int {
	var aInt, _ = strconv.Atoi(aString)
	return aInt
}

// "Center Call Print"
func NSLog(a ...interface{}) {
	time := GetCurrentTime("a").Format("2006/01/02 15:04:05")
	fmt.Print("[", time, "]  ")
	fmt.Print(a...)
	fmt.Print("\n")
}
