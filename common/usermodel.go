package common

import (
	"net/http"

	. "../Helper"
	. "../models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

// ========================================== UserModels ==========================================

// GET All UserModels
func GetAllUserModels(w http.ResponseWriter, r *http.Request) {
	usermodels, err := dao.FindAllUserModel()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	for i, usermodel := range usermodels {
		NSLog("GetAllUserModels ", i, " - ", usermodel.Name, ", ID : ", usermodel.ID)
	}

	respondWithJson(w, http.StatusOK, usermodels)
}

// GET a UserModels by ID
func GetUserModel(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	usermodel, err := dao.FindUserModelById(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid UserModels")
		return
	}
	NSLog("User ", usermodel.Name, " Created, ID : ", params["id"])
	respondWithJson(w, http.StatusOK, usermodel)
}

// Create a new UserModel
func CreateUserModel(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var usermodel = GetUserModelFromRequest(r)
	usermodel.ID = bson.NewObjectId()
	usermodel.CreateAt = GetCurrentTime("createat")

	if err := dao.InsertUserModel(usermodel); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	NSLog("User ", usermodel.Name, " Created, ID : ", usermodel.ID)
	respondWithJson(w, http.StatusCreated, usermodel)
}

// Update a existing UserModel
func UpdateUserModel(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var usermodel = GetUserModelFromRequest(r)
	params := mux.Vars(r)
	usermodel.ID = bson.ObjectIdHex(params["id"])
	usermodel.UpdateAt = GetCurrentTime("createat")

	if err := dao.UpdateUserModel(usermodel); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	NSLog("User ", usermodel.Name, " Updated, ID : ", usermodel.ID)
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

// ========================================== COMMON FUNCTIONS ==========================================

func CreateNewUserModel() UserModel {
	var usermodel UserModel
	usermodel.ID = bson.NewObjectId()
	usermodel.CreateAt = GetCurrentTime("createat")
	return usermodel
}

// Get UserModel From Request
func GetUserModelFromRequest(r *http.Request) UserModel {

	var usermodel = UserModel{
		// ID:              r.FormValue("uniqueUserID"),
		DeviceType:      r.FormValue("deviceType"),
		DeviceID:        r.FormValue("deviceID"),
		Name:            r.FormValue("name"),
		Gender:          r.FormValue("gender"),
		Age:             GetIntFromString(r.FormValue("age")),
		Telephone:       r.FormValue("telephone"),
		TelegramID:      r.FormValue("telegramID"),
		FacebookFdCount: GetIntFromString(r.FormValue("facebookFdCount")),
		FacebookID:      r.FormValue("facebookID"),
		Status:          GetIntFromString(r.FormValue("status")),
		ShowInRentList:  GetIntFromString(r.FormValue("showInRentList")),

		WorkUserModel: WorkUserModel{
			WorkType:        r.FormValue("workType"),
			WorkSalaryRange: GetIntFromString(r.FormValue("workSalaryRange")),
			WorkPlace:       r.FormValue("workPlace"),
			WorkMinTime:     r.FormValue("workMinTime"),
			WorkMaxTime:     r.FormValue("workMaxTime"),
			WorkWeek:        r.FormValue("workWeek"),
		},
		UserRentModel: UserRentModel{
			RentExpectMinCost: GetIntFromString(r.FormValue("rentExpectMinCost")),
			RentExpectMaxCost: GetIntFromString(r.FormValue("rentExpectMaxCost")),
			RentPlace:         r.FormValue("rentPlace"),
			RentDuration:      r.FormValue("rentDuration"),
		},

		// LastOnlineAt: r.FormValue("lastOnlineAt"),
		// UpdateAt:     r.FormValue("updateAt"),
		// CreateAt:     GetCurrentTime("createat"),
	}
	return usermodel
}
