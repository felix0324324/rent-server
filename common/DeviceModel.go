package common

import (
	"net/http"

	. "../Helper"
	. "../models"
	"gopkg.in/mgo.v2/bson"
)

// ==========================================  ==========================================

func GetAllDeviceModel(w http.ResponseWriter, r *http.Request) {
	deviceModels, err := dao.FindAllDeviceModel()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, deviceModels)
}

func GetOrCreateDeviceModel(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var aUserDetail UserDetail
	aUserDetail.ResponseModel = GetResponseModel()
	var deviceID = r.FormValue("deviceID")
	var facebookID = r.FormValue("facebookID")

	deviceModel, err := dao.FindDeviceModelByDeviceID(deviceID)
	if err != nil {
		// Step 2. Device Record Not Found

		var aDeviceModel DeviceModel
		aDeviceModel.ID = bson.NewObjectId()
		aDeviceModel.DeviceID = deviceID
		aDeviceModel.FacebookID = facebookID
		aDeviceModel.CreateAt = GetCurrentTime("createat")

		if len(deviceID) == 0 || len(facebookID) == 0 {
			// NSLog("DeviceModel GetOrCreateDeviceModel - Step 2")
			respondWithError(w, http.StatusInternalServerError, "Device/Facebook ID cant null")
			return
		}

		if err := dao.InsertDeviceModel(aDeviceModel); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		aUserDetail.DeviceModel = aDeviceModel

	} else {
		// Step 2. Found Exists Device Record
		aUserDetail.DeviceModel = deviceModel
	}

	// Step 3. Find UserModel by Facebook ID
	userModel, err := dao.FindUserModelByFacebookID(facebookID)
	if err != nil {
		// Step 4. UserModel Not Found
		var newUserModel = CreateNewUserModel()
		newUserModel.FacebookID = facebookID
		if err := dao.InsertUserModel(newUserModel); err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		aUserDetail.UserModel = newUserModel
		NSLog("DeviceModel GetOrCreateDeviceModel - Step 4-1")
	} else {
		// Step 4. UserModel Found
		aUserDetail.UserModel = userModel
		NSLog("DeviceModel GetOrCreateDeviceModel - Step 4-2")
	}

	respondWithJson(w, http.StatusCreated, aUserDetail)
	// deviceModel = MergeDeviceModelFromRequest(deviceModel, r)
	//deviceModel.ID = bson.NewObjectId()
	//deviceModel.CreateAt = GetCurrentTime("createat")

	// if err := dao.InsertUserModel(deviceModel); err != nil {
	// 	respondWithError(w, http.StatusInternalServerError, err.Error())
	// 	return
	// }

}
