package dao

import (
	"log"

	. "../models"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UsersDAO struct {
	Server   string
	Database string
}

var db *mgo.Database

const (
	CONST_USER   = "users"
	CONST_DEVICE = "device"
)

// Establish a connection to database
func (m *UsersDAO) Connect() {
	session, err := mgo.Dial(m.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

// ========================================== DeviceModel ==========================================

func (m *UsersDAO) FindAllDeviceModel() ([]DeviceModel, error) {
	var deviceModel []DeviceModel
	err := db.C(CONST_DEVICE).Find(bson.M{}).All(&deviceModel)
	return deviceModel, err
}

func (m *UsersDAO) FindDeviceModelByDeviceID(deviceID string) (DeviceModel, error) {
	var deviceModel DeviceModel
	err := db.C(CONST_DEVICE).Find(bson.M{"deviceID": deviceID}).One(&deviceModel)
	return deviceModel, err
}

func (m *UsersDAO) InsertDeviceModel(deviceModel DeviceModel) error {
	err := db.C(CONST_DEVICE).Insert(&deviceModel)
	return err
}

// ========================================== UserModels ==========================================

// Find All UserModel From DB
func (m *UsersDAO) FindAllUserModel() ([]UserModel, error) {
	var userModels []UserModel
	err := db.C(CONST_USER).Find(bson.M{}).All(&userModels)
	return userModels, err
}

// Find UserModel From DB
func (m *UsersDAO) FindUserModelById(id string) (UserModel, error) {
	var userModel UserModel
	err := db.C(CONST_USER).FindId(bson.ObjectIdHex(id)).One(&userModel)
	return userModel, err
}

// Find UserModel From Facebook ID
func (m *UsersDAO) FindUserModelByFacebookID(id string) (UserModel, error) {
	var userModel UserModel
	err := db.C(CONST_USER).Find(bson.M{"facebookID": id}).One(&userModel)
	return userModel, err
}

// Insert UserModel To DB
func (m *UsersDAO) InsertUserModel(userModel UserModel) error {
	err := db.C(CONST_USER).Insert(&userModel)
	return err
}

// Update an existing user
func (m *UsersDAO) UpdateUserModel(userModel UserModel) error {
	err := db.C(CONST_USER).UpdateId(userModel.ID, &userModel)
	return err
}

// ========================================== User ==========================================

// Find list of users
func (m *UsersDAO) FindAll() ([]User, error) {
	var users []User
	err := db.C(CONST_USER).Find(bson.M{}).All(&users)
	return users, err
}

// Find a user by its id
func (m *UsersDAO) FindById(id string) (User, error) {
	var user User
	err := db.C(CONST_USER).FindId(bson.ObjectIdHex(id)).One(&user)
	return user, err
}

// Insert a user into database
func (m *UsersDAO) Insert(user User) error {
	err := db.C(CONST_USER).Insert(&user)
	return err
}

// Delete an existing user
func (m *UsersDAO) Delete(user User) error {
	err := db.C(CONST_USER).Remove(&user)
	return err
}

// Update an existing user
func (m *UsersDAO) Update(user User) error {
	err := db.C(CONST_USER).UpdateId(user.ID, &user)
	return err
}
