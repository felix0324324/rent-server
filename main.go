package main

import (
	"log"
	"net/http"
	. "./common"
	"github.com/gorilla/mux"
)

// Define HTTP request routes
func main() {
	r := mux.NewRouter()

	// r.HandleFunc("/v1", handler)
	// r.HandleFunc("/users", AllUsersEndPoint).Methods("GET")
	// r.HandleFunc("/users", CreateUserEndPoint).Methods("POST")
	// r.HandleFunc("/users/{id}", UpdateUserEndPoint).Methods("PUT")
	// r.HandleFunc("/users", DeleteUserEndPoint).Methods("DELETE")
	// r.HandleFunc("/users/{id}", FindUserEndpoint).Methods("GET")

	r.HandleFunc("/v1/GetDevice", GetOrCreateDeviceModel).Methods("POST")
	r.HandleFunc("/v1/GetDevice/all", GetAllDeviceModel).Methods("GET")

	r.HandleFunc("/v1/usermodel", CreateUserModel).Methods("POST")
	r.HandleFunc("/v1/usermodel/all", GetAllUserModels).Methods("GET") // should hidden in production
	r.HandleFunc("/v1/usermodel/{id}", GetUserModel).Methods("GET")
	r.HandleFunc("/v1/usermodel/{id}", UpdateUserModel).Methods("PUT")

	// r.HandleFunc("/v1/rentmodel", CreateRentModel).Methods("POST")
	// r.HandleFunc("/v1/rentmodel/{id}", GetRentModel).Methods("GET")
	// r.HandleFunc("/v1/usermodel/all", GetAllRentModels).Methods("GET")

	if err := http.ListenAndServe(":3000", r); err != nil {
		log.Fatal(err)
	}
}

// ========================================== Users ==========================================
/*
// GET list of users
func AllUsersEndPoint(w http.ResponseWriter, r *http.Request) {
	users, err := dao.FindAll()
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, users)
}

// GET a users by its ID
func FindUserEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	user, err := dao.FindById(params["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid user ID")
		return
	}
	respondWithJson(w, http.StatusOK, user)
}

// POST a new user
func CreateUserEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		fmt.Printf("Error Reason : '%s'\n", err)
		respondWithError(w, http.StatusBadRequest, "Invalid request payload1")
		return
	}
	user.ID = bson.NewObjectId()
	if err := dao.Insert(user); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusCreated, user)
}

// PUT update an existing user
func UpdateUserEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	var user User
	user.ID = bson.ObjectIdHex(params["id"])
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload2")
		return
	}
	if err := dao.Update(user); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

// DELETE an existing user
func DeleteUserEndPoint(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload3")
		return
	}
	if err := dao.Delete(user); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}
*/
